# Example Time Tracking add-on

This add-on demonstrates the use of the _jiraTimeTrackingProviders_ module in Connect.
It was built using the Atlassian Connect Express web application framework.

## Running

Refer to the [Atlassian Connect Express docs](https://bitbucket.org/atlassian/atlassian-connect-express/src/master/README.md#markdown-header-install-dependencies).
